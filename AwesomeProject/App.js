import React, { Component } from 'react';
import { AppRegistry, Text, StyleSheet, View } from 'react-native';

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {isShowingText: true};

    // Toggle the state every second
    setInterval(() => {
      this.setState(previousState => {
        return { isShowingText: !previousState.isShowingText };
      });
    }, 1000);
  }

  render() {
    let display = this.state.isShowingText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

export default class BlinkApp extends Component {
  render() {
    return (
      <View>
        <Blink text='A worried man with a worried mind' />
        <Blink text='No one in front of me and nothing behind' />
        <Blink text='There’s a woman on my lap and she’s drinking champagne' />
        <Blink text='Got white skin, got assassin’s eyes' />
        <Blink text='I’m looking up into the sapphire-tinted skies' />
        <Blink text='I’m well dressed, waiting on the last train' />
        <Blink text='Standing on the gallows with my head in a noose' />
        <Blink text='Any minute now I’m expecting all hell to break loose' />
        <Blink text='People are crazy and times are strange' />
        <Blink text='I’m locked in tight, I’m out of range' />
        <Blink text='I used to care, but things have changed' />
      </View>
    );
  }
}
const styles = StyleSheet.create({
    red:{
      color:'red',
    },
    // lawngreen:{
    //   color:'lawngreen '
    // },
    // purple :{
    //   color:'purple '
    // },
    // bigblue:{
    //   color:'blue',
    //   fontWeight:'bold',
    // },
  });
// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => BlinkApp);
